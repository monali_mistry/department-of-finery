module.exports = function(grunt) {
 
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-shopify');
 
	  grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
        conf: grunt.file.readJSON('config.json'),
		  
	    shopify: {
	      options: {
	        //From the config.json
            api_key: "<%= conf.APIKey %>",
            password: "<%= conf.Password %>",
            url: "<%= conf.SiteUrl %>",
            theme: "<%= conf.ThemeId %>",
            base: 'shop/'
	      }
	    },
	    
	    
	   //Sass
	    sass: {
			 dist:{
				 options:{
					style:'compressed',
					sourcemap:'none'
				 },
				 files:[{
					src:'dev/scss/app.scss',
					dest:'shop/assets/theme.min.css'
				 }]
			 }
		 },
	   
		//Compress the JS
		uglify: {
		      options: {
		        banner: '/*!-- <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy HH:MM:ss") %> --*/\n'
		      },
		      
		      dist : {
					src : 'dev/js/*.js',
					dest : 'shop/assets/scripts.min.js'
				}
		},
	    
	    watch: {
	    	 options:{
			 spawn:false,
			 livereload:35729
		 },
		 scripts:{
			files:['dev/scss/*.scss','dev/js/*.js'],
		 	tasks:['sass','uglify']
		 },
	      shopify: {
	        files: ['shop/**'],
	        tasks: ["shopify"]
	      }
	    },	    
	    
	 
	  });
	  
	  grunt.registerTask('default', ['sass','uglify','shopify','watch']);
 
};
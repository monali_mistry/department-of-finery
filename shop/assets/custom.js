(function($) {
  	
	$(document).ready(function() {		
		planetx.init();
	});
	
	var planetx = {
		
		init: function() {
			this.Newsletter();
			this.Product_slider();
			this.Accordion();
			this.mobile.init();
			this.Header();
			this.qty();
		},
		
		Header: function(){
			
			// ------------------ Search -------------------
			
			/* if($(window).width() >= 768){
				$('.search-icon').click(function(e) {
					e.preventDefault();
					$('.search_opened').addClass('active');
				});
			} */
			
			
			if($(window).width() <= 1024){
				$('.search-icon').click(function(e) {
					e.preventDefault();
					$('.mob_search').slideToggle();
				});
			}
			
			// ------------------ Sticky Nav -------------------
			
			if($(window).width() >= 1300){
				$(window).scroll(function() {    
					var scroll = $(window).scrollTop();
					if (scroll >= 162) {
						$("header").addClass("active");
					} else {
						$("header").removeClass("active");
					}
				});
			}
			
			// ------------------ Blak Header DIV ------------------
			
			if($(window).width() <= 1024){
				$('.mob_blank_header').css('height', $('.mean-bar').outerHeight() - 2);
			}
			
		},
		
		Product_slider: function(){
			
			// ------------------ Product Slider -------------------
			
			$('#product-pager').bxSlider({
			  mode: 'horizontal',
			  pager: false,
			  controls: true,
			  minSlides: 2,
			  maxSlides: 2,
			  slideWidth: 206,
			  slideMargin: 12,
			  infiniteLoop: false,
			});
		},
		
		Accordion: function(){
			$( "#accordion_content" ).accordion({
			  collapsible: true,
			  heightStyle: "content"
			});
			
			$( "#stockist_accordion" ).accordion({
			  collapsible: true,
			  heightStyle: "content"
			});
		},
		
		Newsletter: function(){
			
			setTimeout(function(){
				//$('.newsletter_popup').addClass('active');
			}, 1500);
            
			
			$('.close_popup').click(function(e) {
                e.preventDefault();
				$('.newsletter_popup').removeClass('active');
            });
		},
		
		qty:function() {
          //on cart page
            $("body").on('click', '.js-qty__adjust--plus', function() {			
				
				$(".update-cart")[0].click();
				//alert(getval);
				
			});
          	$("body").on('click', '.js-qty__adjust--minus', function() {
				
					

					$(".update-cart")[0].click();
				
			});
        }, 
		
		// ----------- For Mobile ------------
		mobile: {
			init: function() {
				this.mobileNavTrigger();
			},
			
			mobileNavTrigger: function(){	
				$('header nav#main-nav').meanmenu();
			}
		},
		
          
	}
        
})(jQuery);

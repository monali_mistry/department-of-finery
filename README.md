# Hulkcode Shopify Base Repository

## Setup the configuration file - config.json

Follow the steps in order to setup your grunt configuration (Grunt - Shopify Plugin):

1. Navigate to Apps - Private Apps - Create a Private App
2. Create an App titled DevApp
3. Copy - paste the **API Key** and **Password** to **config.json**
4. Enter the shopify site url, for example test.myshopify.com (exclude https://)

### Enter the Theme ID

1. Navigate to Online Store - Themes
2. Duplicate/Create new theme

    + If you want to create a new theme, add shop folder to archive shop.zip and uploaded it
    
    + If you want to modify an existing theme,
        **duplicate** the theme and download the theme file.
        Replace the contents of shop folder with the duplicated theme folders
    
3. Click the eye icon to preview your duplicated/new theme and copy-paste the **preview_theme_id** from the url. Altenatively you can use your console - https://github.com/wilr/grunt-shopify#theme-optional


config.json example :
```
{
    "APIKey" : "6f5cca32c4edea3be53503623588044f", 
    "Password" : "2ebcd1546af52c192abfe34b217361e1", 
    "SiteUrl" : "my-test-site.myshopify.com", 
    "ThemeId" : "77539395"
}
```

For more information please visit - https://github.com/wilr/grunt-shopify

## The **shop** folder

**Shop** folder contains all the necessary Shopify theme files. This is the folder that’s uploaded to Shopify each time you save your project.

Right now the **shop** folder is a duplication of Timber Frameword (v.2.1.4) - http://shopify.github.io/Timber/

The only difference is that **layout/theme.liquid**, includes two minified files :

+ **theme.min.css** (at the head tag)

+ **scripts.min.css** (at the end of the body tag)

and that **timber.scss.liquid** uses different breakpoints (taken from Bootstrap):

+ $small: 767px;

+ $medium: 991px;

+ $large: 992px;

## The **dev** folder

You write all your CSS and Javascript code, inside **dev/scss/** and **dev/js/** folders, accordingly.
There is **no need** to include any extra files inside your theme.liquid.

### theme.min.css
The **theme.min.css** file is generated from **dev/scss/app.scss**.
Inside **dev/scss** folder you can create as many .scss files as you want. Finally, all you need to do, is **import** them to **app.scss**. 

Feel free to import variables, mixins, separate **.scss** files for every template (product.liquid, collection.liquid ) etc. In the end, all the files will be combined and minified into one file (**theme.min.css**).

### scripts.min.css
The **scripts.min.css file** is generated from the combination of **dev/js/libs.js** and **dev/js/script.js** files. 

+ **libs.js** include all your library files

+ **script.js** include all your custom javascript

**Note:** Inside **dev/js** folder, you can create as many **.js** files as you want. In the end, all the files will be combined and minified into one file (**scripts.min.js**).